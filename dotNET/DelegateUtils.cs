﻿using System;

namespace xshare
{

    
    public static class DelegateUtils
    {


        public static void SafeInvokeDelegate<T>(Delegate d, T arg1)
        {
            if (null == d)
                return;
            try
            {
                ((Action<T>)d).Invoke(arg1);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }
        public static void SafeInvoke<T>(Action<T> a, T arg1)
        {
            if (null == a)
                return;
            try
            {
                a.Invoke(arg1);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }


        public static void SafeInvokeDelegate<T1, T2>(Delegate d, T1 arg1, T2 arg2)
        {
            if (null == d)
                return;
            try
            {
                ((Action<T1, T2>)d).Invoke(arg1, arg2);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }
        public static void SafeInvoke<T1, T2>(Action<T1, T2> a, T1 arg1, T2 arg2)
        {
            if (null == a)
                return;
            try
            {
                a.Invoke(arg1, arg2);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }


        public static void SafeInvokeDelegate<T1, T2, T3>(Delegate d, T1 arg1, T2 arg2, T3 arg3)
        {
            if (null == d)
                return;
            try
            {
                ((Action<T1, T2, T3>)d).Invoke(arg1, arg2, arg3);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }
        public static void SafeInvoke<T1, T2, T3>(Action<T1, T2, T3> a, T1 arg1, T2 arg2, T3 arg3)
        {
            if (null == a)
                return;
            try
            {
                a.Invoke(arg1, arg2, arg3);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }


        public static void SafeInvokeDelegate<T1, T2, T3, T4>(Delegate d, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (null == d)
                return;
            try
            {
                ((Action<T1, T2, T3, T4>)d).Invoke(arg1, arg2, arg3, arg4);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }
        public static void SafeInvoke<T1, T2, T3, T4>(Action<T1, T2, T3, T4> a, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (null == a)
                return;
            try
            {
                a.Invoke(arg1, arg2, arg3, arg4);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }


        public static void SafeInvokeDelegate<T1, T2, T3, T4, T5>(Delegate d, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            if (null == d)
                return;
            try
            {
                ((Action<T1, T2, T3, T4, T5>)d).Invoke(arg1, arg2, arg3, arg4, arg5);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }
        public static void SafeInvoke<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> a, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            if (null == a)
                return;
            try
            {
                a.Invoke(arg1, arg2, arg3, arg4, arg5);
            }
            catch (Exception ex)
            {
                Log.d($"exception: {ex.GetType().Name}, {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }


    } //end of class


}