﻿

namespace xshare
{

    public struct FileChecksum
    {
        /// <summary>
        /// crc32或md5
        /// </summary>
        public string type;

        public long len;

        public string checksum;

    } //end of class


}
