﻿using System.Threading;
using System;

namespace xshare
{


    public static class ThreadUtils
    {

        public static void SafeSleep(int millis)
        {
            try
            {
                Thread.Sleep(millis);
            }
            catch (Exception ex)
            {
                Log.d($"ThreadUtils: SafeSleep: exception: {ex.GetType().Name}: {ex.Message}");
                Log.e(ex.StackTrace);
            }
        }

    } //end of class


}
