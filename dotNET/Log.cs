﻿#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_ANDROID || UNITY_IOS
using UnityEngine;

namespace xshare
{

    public static class Log
    {

        public static void d(string msg)
        {
            Debug.Log(msg);
        }

        public static void w(string msg)
        {
            Debug.LogWarning(msg);
        }

        public static void e(string msg)
        {
            Debug.LogError(msg);
        }

    } //end of class

}

#else
using System;

namespace xshare
{

    public static class Log
    {

        public static void d(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void w(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void e(string msg)
        {
            Console.WriteLine(msg);
        }

    } //end of class

}


#endif

