﻿using System.Text;

namespace xshare
{


    public static class PathUtils
    {


        public static string CombileDir(string dir1, string dir2)
        {
            var sb = StringUtils.Builder;

            sb.Append(dir1);
            if (!IsLastCharSeparator(sb))
                sb.Append('/');

            sb.Append(dir2);
            if (!IsLastCharSeparator(sb))
                sb.Append('/');

            return sb.BuildString();
        }


        public static bool IsFirstCharSeparator(string s)
        {
            var c = s[0];
            if ('\\' == c || '/' == c)
                return true;

            return false;
        }
        public static bool IsFirstCharSeparator(StringBuilder sb)
        {
            var c = sb[0];
            if ('\\' == c || '/' == c)
                return true;

            return false;
        }


        public static bool IsLastCharSeparator(StringBuilder sb)
        {
            var endIndex = sb.Length - 1;
            var c = sb[endIndex];
            if ('\\' == c || '/' == c)
                return true;

            return false;
        }
        public static bool IsLastCharSeparator(string s)
        {
            var endIndex = s.Length - 1;
            var c = s[endIndex];
            if ('\\' == c || '/' == c)
                return true;

            return false;
        }

        /// <summary>
        /// 将路径统一为以/分隔
        /// </summary>
        public static string CheckSeparator(string dir)
        {
            return StringUtils.Builder.Append(dir).Replace('\\', '/').BuildString();
        }


        public static StringBuilder DirEndWithSeparator(StringBuilder sb)
        {
            var endIndex = sb.Length - 1;
            var endChar = sb[endIndex];
            if ('/' == endChar)
                return sb;

            if ('\\' != endChar)
                sb.Append('/');
            else
                sb[endIndex] = '/';

            return sb;
        }

        /// <summary>
        /// 返回以/结尾的路径
        /// </summary>
        public static string DirEndWithSeparator(string dir)
        {
            var endIndex = dir.Length - 1;
            var endChar = dir[endIndex];
            if ('/' == endChar)
                return dir;

            var sb = StringUtils.Builder;
            sb.Append(dir);
            if ('\\' != endChar)
                sb.Append('/');
            else
                sb[endIndex] = '/';

            return sb.BuildString();
        }


    } //end of class


}
