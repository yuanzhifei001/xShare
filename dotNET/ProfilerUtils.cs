﻿using System.Diagnostics;

namespace xshare
{

    public static class ProfilerUtils
    {

#if UNITY_EDITOR

        static Stopwatch s_sw = new Stopwatch();

        public static void SwBegin()
        {
            s_sw.Restart();
        }

        public static void SwEndAndDump(string tag, long timeout = -1)
        {
            s_sw.Stop();
            var ms = s_sw.ElapsedMilliseconds;

            if (timeout > 0)
            {
                if (ms > timeout)
                    UnityEngine.Debug.LogError($"use time: {tag}, {ms}ms");
            }
            else
            {
                UnityEngine.Debug.Log($"use time: {tag}, {ms}ms");
            }
        }
#else

        [Conditional("DEBUG")]
        public static void SwBegin()
        {
        }

        [Conditional("DEBUG")]
        public static void SwEndAndDump(string tag, long timeout)
        {
        }

#endif

    } //end of class


}

