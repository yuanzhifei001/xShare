﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace xshare
{

    /// <summary>
    /// 主线程(渲染线程)定时器. 注意: 不要在异步线程上使用该类
    /// </summary>
    public class Scheduler
    {

        static Pool<Scheduler> s_pool;
        static List<Scheduler> s_runningSchedulerList; //正在运行的带tag的Scheduler

        static Scheduler Obtain()
        {
            if (null == s_pool)
                s_pool = new Pool<Scheduler>((sender) => new Scheduler(), (sender, msg) => { msg.Reset(); });

            var ret = s_pool.Obtain();
            return ret;
        }

        public static void ScheduleDelay(float delay, Action<bool, object, Scheduler> selector, object tag)
        {
            var timer = Obtain();

            timer.delay = delay;
            timer.selector = selector;
            timer._tag = tag;
            timer._autoRecycle = true;

            timer.Start();
        }

        public static Scheduler ObtainDelay(float delay, Action<bool, object, Scheduler> selector, List<Scheduler> inRunList = null)
        {
            var timer = Obtain();

            timer.delay = delay;
            timer.selector = selector;
            timer._inRunList = inRunList;
            timer._autoRecycle = (null != inRunList);

            return timer;
        }

        public static Scheduler ObtainRepeat(float interval, int loop, Action<bool, object, Scheduler> selector)
        {
            var timer = Obtain();

            timer.delay = 0;
            timer.interval = interval;
            timer.loop = loop;
            timer.selector = selector;

            return timer;
        }

        public static Scheduler Obtain(float delay, float interval, int loop, Action<bool, object, Scheduler> selector)
        {
            var timer = Obtain();

            timer.delay = delay;
            timer.interval = interval;
            timer.loop = loop;
            timer.selector = selector;

            return timer;
        }

        public static void StopAndRecycle(Scheduler s)
        {
            if (null == s)
                return;

            s.Stop();
            Recycle(s);
        }

        public static void Recycle(Scheduler s)
        {
            if (null == s_pool)
                return;

            s_pool.Recycle(s);
        }

        public static void StopAllAndRecycle(List<Scheduler> list)
        {
            if (null == list)
                return;

            for (var i = list.Count - 1; i >= 0; --i)
            {
                var sche = list[i];
                sche._inRunList = null;
                sche.Stop();
                Scheduler.Recycle(sche);
            }
            list.Clear();
        }


        //==========


        public static void StopAllByTag(object tag)
        {
            if (null == s_runningSchedulerList)
                return;

            for (var i = s_runningSchedulerList.Count - 1; i >= 0; --i)
            {
                var s = s_runningSchedulerList[i];
                if (null == s)
                    continue;

                if (s._tag == tag)
                {
                    s._tag = null; //防止Stop函数内操作 s_runningSchedulerList
                    s.Stop();
                    s._tag = tag;
                }
            }
            s_runningSchedulerList.ClearNull();
        }


        //==========


        public Action<bool, object, Scheduler> selector;

        object _tag = null; //运行时设置tag没有任何效果

        /// <summary>
        /// 执行延迟
        /// </summary>
        public float delay;

        /// <summary>
        /// 执行间隔(允许运行中修改)
        /// </summary>
        public float interval;

        /// <summary>
        /// 执行次数
        /// </summary>
        public int loop;

        public bool timeScale = true;

        /// <summary>
        /// 执行结束时自动回收
        /// </summary>
        bool _autoRecycle;

        object _userData;

        float _leftTime;
        float _leftLoop;
        bool _running;
        public bool IsRunning { get { return _running; } }

        /// <summary>
        /// 用于在某个界面关闭时stop还未执行完的Scheduler.
        /// </summary>
        List<Scheduler> _inRunList;

        protected Scheduler()
        {
            _OnUpdate = OnUpdate;
        }

        void Reset()
        {
            _tag = null;
            selector = null;
            delay = 0;
            interval = 0;
            loop = 0;
            timeScale = true;
            _autoRecycle = false;
            _userData = null;

            _leftTime = 0;
            _leftLoop = 0;
            _running = false;
            _inRunList = null;
        }

        public Scheduler SetAutoRecycle(bool b)
        {
            if (_running)
                Log.d($"Scheduler: set ignore when running");
            else
                _autoRecycle = b;

            return this;
        }

        public void InitTag(object tag)
        {
            if (_running)
            {
                Log.d($"Scheduler: running, tag init ignore");
                return;
            }
            if (null != _tag)
            {
                Log.d($"Scheduler: tag have init");
                return;
            }

            _tag = tag;
        }

        public bool Start(object userData = null)
        {
            if (_running)
                return false;

            _userData = userData;
            _leftTime = delay + interval;
            _leftLoop = loop;

            _Start();
            return true;
        }

        public void Restart(object userData = null)
        {
            _userData = userData;
            _leftTime = delay + interval;
            _leftLoop = loop;

            if (_running)
                return;

            _Start();
        }

        void _Start()
        {
            if (s_pool.Contains(this))
            {
                Log.e($"Scheduler has recycle");
                return;
            }

            if (null != _inRunList)
                _inRunList.Add(this);

            //开始运行的时候(Start, Resume), 如果有tag, 则加入dict; 停止的时候移除
            if (null != _tag)
            {
                if (null == s_runningSchedulerList)
                    s_runningSchedulerList = new List<Scheduler>();

                s_runningSchedulerList.Add(this);
            }

            _running = true;
            xBehaviour.OnUpdate += _OnUpdate;
        }

        public bool Stop()
        {
            if (!_running)
                return false;

            _running = false;
            xBehaviour.OnUpdate -= _OnUpdate;

            if (null != _inRunList)
                _inRunList.Remove(this);

            if (null != _tag)
            {
                for (var i = 0; i < s_runningSchedulerList.Count; ++i)
                {
                    var s = s_runningSchedulerList[i];
                    if (s == this)
                        s_runningSchedulerList[i] = null;
                }
                s_runningSchedulerList.ClearNull();
            }

            return true;
        }

        public void Resume()
        {
            if (_running)
                return;

            if (_leftTime <= 0 && 0 == _leftLoop) //已执行完没必要恢复了
                return;

            _Start();
        }

        Action<float> _OnUpdate;
        void OnUpdate(float deltaTime)
        {
            if (!_running)
            {
                xBehaviour.OnUpdate -= _OnUpdate;
                return;
            }

            var dt = timeScale ? Time.deltaTime : Time.unscaledDeltaTime;
            _leftTime -= dt;

            var leftTime = _leftTime;
            if (leftTime > 0)
                return;

            var isEnd = false;

            if (_leftLoop > 0)
                _leftLoop -= 1;

            if (0 == _leftLoop)
            {
                Stop();
                isEnd = true;
            }
            else if (_leftLoop < 0) //无限循环
            {
                //do nothing
            }

            DelegateUtils.SafeInvoke(selector, isEnd, _userData, this);
            if (isEnd)
            {
                if (_running) //在selector中调用了Start的情况
                {
                    Log.d($"Scheduler: call start in selector");
                }
            }
            else
            {
                if (!_running) //在selector中调用了Stop的情况
                {
                    Log.d($"Scheduler: call stop in selector");
                    isEnd = true;
                }
            }

            if (isEnd)
            {
                _leftTime = 0;
                if (_autoRecycle)
                {
                    Log.d($"Scheduler end: autoRecycle");
                    Scheduler.Recycle(this);
                }
            }
            else
            {
                if (false) Log.d($"Scheduler: next loop: {_leftTime}, {interval}");
                _leftTime += interval; //下一个loop(在selector中修改执行间隔也没有影响)
                if (_leftTime < 0)
                    _leftTime = 0;
            }
        }


    } //end of class


}
